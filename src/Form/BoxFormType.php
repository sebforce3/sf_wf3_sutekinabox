<?php

namespace App\Form;

use App\Entity\Box;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BoxFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => "Nom de la box",
                'attr' => ['placeholder' => "Un joli nom"]
            ])
            ->add('image',FileType::class, [
                'required' => true,
                'label' => "Image box",
                'attr' => ['placeholder' => "Une très belle image"]
            ])
            ->add('flavor', ChoiceType::class, [
                'label' => "Choisir le modèle",
                'choices'  => ['Small' => 'small', 'Medium' => 'medium', 'Large' => 'large'],
                'preferred_choices' => ['Large' => 'large']
            ])
            ->add('releaseDate', DateType::class, [
                'label' => "Date de sortie souhaitée",
                'widget' => 'single_text',
                'html5' => false,
                'attr' => ['class' => 'js-datepicker'],
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Envoyer la Sutekina !"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Box::class,
        ]);
    }
}
