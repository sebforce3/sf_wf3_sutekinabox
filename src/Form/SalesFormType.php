<?php

namespace App\Form;

use App\Entity\SalesManager;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SalesFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => "Nom",
                'attr' => [
                    'placeholder' => "Indiquez votre nom"
                ]
            ])
            ->add('email', TextType::class, [
                'required' => true,
                'label' => "Courriel",
                'attr' => [
                    'placeholder' => "Indiquez votre courriel"
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label' => "Rejoignez l'équipe des ventes de Sutekina Box"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SalesManager::class,
        ]);
    }
}
