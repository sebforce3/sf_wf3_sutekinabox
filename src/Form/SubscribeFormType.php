<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 06/12/2018
 * Time: 09:56
 */

namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SubscribeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => "Nom",
                'attr' => [
                    'placeholder' => "Placide Muzeau"
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => "Saisissez votre courriel",
                'attr' => [
                    'placeholder' => "cool@lalala.com"
                ]
            ])
            ->add('password', PasswordType::class, [
                'required' => true,
                'label' => "Saisissez votre mot de passe",
                'attr' => [
                    'placeholder' => "aïe, aïe"
                ]
            ])
            ->add('roles', ChoiceType::class, [
                'label' => "Choisir votre rôle",
                'choices'  => ['Créateur de box' => 'ROLE_MARKETING_DESIGNER',
                    'Responsable des ventes' => 'ROLE_SALES_MANAGER',
                    'Fournisseur' => 'ROLE_PRODUCT_SUPPLIER'],
                'preferred_choices' => ['Créateur de box' => 'ROLE_MARKETING_DESIGNER']
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Welcome !'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null
        ]);
    }
}