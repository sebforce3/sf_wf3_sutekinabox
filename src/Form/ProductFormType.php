<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => "Nom de l'article",
                'attr' => ['placeholder' => "Nom de l'article"]
            ])
            ->add('image',FileType::class, [
                'required' => true,
                'label' => "Importer l'image de votre article",
                'attr' => ['placeholder' => "Image 50x50 de préférence"]
            ])
            ->add('description', TextType::class, [
                'required' => true,
                'label' => "Décrivez votre produit",
                'attr' => ['placeholder' => "Cette description figurera sur notre site"]
            ])
            ->add('price')
            ->add('submit', SubmitType::class, [
                'label' => "Ajouter l'article"
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
