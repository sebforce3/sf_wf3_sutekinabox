<?php

namespace App\Repository;

use App\Entity\MarketingDesigner;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MarketingDesigner|null find($id, $lockMode = null, $lockVersion = null)
 * @method MarketingDesigner|null findOneBy(array $criteria, array $orderBy = null)
 * @method MarketingDesigner[]    findAll()
 * @method MarketingDesigner[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MarketingDesignerRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MarketingDesigner::class);
    }

    // /**
    //  * @return MarketingDesigner[] Returns an array of MarketingDesigner objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MarketingDesigner
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
