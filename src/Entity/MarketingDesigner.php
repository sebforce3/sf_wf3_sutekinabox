<?php

namespace App\Entity;

use App\Entity\Box;
use App\Entity\Member;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarketingDesignerRepository")
 */
class MarketingDesigner
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     * @ORM\OneToOne(targetEntity="App\Entity\Member")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $name;

    /**
     *  @ORM\OneToMany(targetEntity="App\Entity\Box", mappedBy="designedBy")
     */
    private $designedBoxes;

    public function __construct(int $id)
    {
        $this->designedBoxes = new ArrayCollection();
        $this->setId($id);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return MarketingDesigner
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return MarketingDesigner
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDesignedBoxes()
    {
        return $this->designedBoxes;
    }

    /**
     * @param mixed $id
     * @return MarketingDesigner
     */
    public function addDesignedBoxes(Box $box)
    {
        $this->designedBoxes[] = $box;
        return $this;
    }
}
