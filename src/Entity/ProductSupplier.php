<?php

namespace App\Entity;

use App\Entity\Product;
use App\Entity\Member;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarketingDesignerRepository")
 */
class ProductSupplier
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     * @ORM\OneToOne(targetEntity="App\Entity\Member")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="suppliers")
     * @JoinTable(name="products",
     *      joinColumns={@JoinColumn(name="supplier_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    private $products;


    public function __construct(int $id)
    {
       $this->products = new ArrayCollection();
       $this->setId($id);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return ProductSupplier
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return ProductSupplier
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;

    }
    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $id
     * @return ProductSupplier
     */
    public function addProduct(Product $product): self
    {
        $this->products[] = $product;
        return $this;
    }
}
