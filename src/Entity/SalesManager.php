<?php

namespace App\Entity;

use App\Entity\Box;
use App\Entity\Product;
use App\Entity\Member;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MarketingDesignerRepository")
 */
class SalesManager
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     * @ORM\OneToOne(targetEntity="App\Entity\Member")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product", mappedBy="approvedBy")
     */
    private $approvedProducts;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Box", mappedBy="approvedBy")
     */
    private $approvedBoxes;

    /**
     * SalesManager constructor.
     */
    public function __construct(int $id)
    {
        $this->approvedProducts = new ArrayCollection();
        $this->setId($id);
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return SalesManager
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return SalesManager
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApprovedProducts()
    {
        return $this->approvedProducts;
    }

    /**
     * @param mixed $approvedProducts
     * @return SalesManager
     */
    public function addApprovedProducts(Product $product)
    {
        $this->approvedProducts[] = $product;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getApprovedBoxes()
    {
        return $this->approvedBoxes;
    }

    /**
     * @param mixed $approvedBoxes
     * @return SalesManager
     */
    public function addApprovedBoxes(Box $box)
    {
        $this->approvedBoxes[] = $box;
        return $this;
    }
}
