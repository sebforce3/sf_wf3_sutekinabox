<?php

namespace App\Entity;

use DateInterval;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\JoinTable;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BoxRepository")
 */
class Box
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $flavor;

    /**
     * @ORM\Column(type="string", length=80)
     */
    private $state;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $releaseDate;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Product", inversedBy="boxes")
     * @JoinTable(name="boxes",
     *      joinColumns={@JoinColumn(name="box_id", referencedColumnName="id")},
     *      inverseJoinColumns={@JoinColumn(name="product_id", referencedColumnName="id")}
     *      )
     */
    private $products;

    /**
     *  @ORM\ManyToOne(targetEntity="App\Entity\MarketingDesigner", inversedBy="designedBoxes")
     *  @ORM\JoinColumn(nullable=false)
     */
    private $designedBy;

    /**
     *  @ORM\ManyToOne(targetEntity="App\Entity\SalesManager", inversedBy="approvedBoxes")
     *
     */
    private $approvedBy;

    /**
     * Box constructor.
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->setReleaseDate($this->nextMonth());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFlavor(): ?string
    {
        return $this->flavor;
    }

    public function setFlavor(string $flavor): self
    {
        $this->flavor = $flavor;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(?string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getReleaseDate(): ?\DateTimeInterface
    {
        return $this->releaseDate;
    }

    public function setReleaseDate(?\DateTimeInterface $releaseDate): self
    {
        $this->releaseDate = $releaseDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param mixed $products
     */
    public function setProducts($products): self
    {
        $this->products = $products;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDesignedBy()
    {
        return $this->designedBy;
    }

    /**
     * @param mixed $designedBy
     * @return Box
     */
    public function setDesignedBy($designedBy)
    {
        $this->designedBy = $designedBy;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getValidateBy()
    {
        return $this->validateBy;
    }

    /**
     * @param mixed $validateBy
     * @return Box
     */
    public function setValidateBy($validateBy)
    {
        $this->validateBy = $validateBy;
        return $this;
    }

    /**
     * Obtenir la date de la fin du mois prochain
     * @return DateTime
     * @throws \Exception
     */
    private function nextMonth() : DateTime
    {
        // faire en sorte que la boite soit programmée pour la fin du mois suivant
        // pour l'instant on se contente de décaler d'un mois jour pour jour
        $release =  new DateTime('now');
        $release->add(new DateInterval("P1M"));
        return $release;
    }


}
