<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 10/12/2018
 * Time: 12:42
 */

namespace App\Controller\SutekinaBox;


use App\Entity\Product;
use App\Form\ProductFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/supplier/add", name="sutekina_supplier_add_product")
     */
    public function addProduct(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductFormType::class, $product)
            ->handleRequest($request);

        # Si le formulaire est soumis et qu'il est valide
        if( $form->isSubmitted() && $form->isValid() ) {

            // 1. Traitement de l'upload de l'image

            /** @var UploadedFile $featuredImage */
            $image = $product->getImage();

            if (null !== $image)
            {
                $fileName = $this->slugify($product->getName())
                    . '.' . $featuredImage->guessExtension();

                try {
                    $featuredImage->move(
                        $this->getParameter('product_assets_dir'),
                        $fileName
                    );
                } catch (FileException $e) {

                }
            }
            else{
                $article->setFeaturedImage($originalfeaturedImage);
            }

            # Mise à jour de l'image
            $article->setFeaturedImage($fileName);

            # 2. Mise à jour du Slug
            $article->setSlug($this->slugify($article->getTitre()));

            # 3. Sauvegarde en BDD
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            # 4. Notification
            $this->addFlash('notice',
                "Votre article a été ajouté !");

            # 5. Redirection vers l'article créé
            return $this->redirectToRoute('index_article', [
                'categorie' => $article->getCategorie()->getSlug(),
                'slug' => $article->getSlug(),
                'id' => $article->getId()
            ]);
        }

        # Affichage du Formulaire
        return $this->render('products/new-product-form.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/sales/validate", name="sutekina_sales_validate")
     */
    public function validate(Request $request)
    {
        // a faire
    }

}
