<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 10/12/2018
 * Time: 12:34
 */

namespace App\Controller\SutekinaBox;


use App\Entity\Box;

use App\Form\BoxFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MakeBoxController extends AbstractController
{
    /**
     * @Route("/marketing/makebox", name="sutekina_marketing_make_box")
     */
    public function makeBox(Request $request)
    {
        $product = new Box();

        $form = $this->createForm(BoxFormType::class, $product)
            ->handleRequest($request);

        # Si le formulaire est soumis et qu'il est valide
        if( $form->isSubmitted() && $form->isValid() ) {

            // 1. Traitement de l'upload de l'image

            /** @var UploadedFile $featuredImage */
            $featuredImage = $article->getFeaturedImage();

            if (null !== $featuredImage)
            {
                $fileName = $this->slugify($article->getTitre())
                    . '.' . $featuredImage->guessExtension();

                try {
                    $featuredImage->move(
                        $this->getParameter('articles_assets_dir'),
                        $fileName
                    );
                } catch (FileException $e) {

                }
            }
            else{
                $article->setFeaturedImage($originalfeaturedImage);
            }

            # Mise à jour de l'image
            $article->setFeaturedImage($fileName);

            # 2. Mise à jour du Slug
            $article->setSlug($this->slugify($article->getTitre()));

            # 3. Sauvegarde en BDD
            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();

            # 4. Notification
            $this->addFlash('notice',
                "L'article a été modifié !");

            # 5. Redirection vers l'article créé
            return $this->redirectToRoute('index_article', [
                'categorie' => $article->getCategorie()->getSlug(),
                'slug' => $article->getSlug(),
                'id' => $article->getId()
            ]);
        }

        # Affichage du Formulaire
        return $this->render('products/new-box-form.html.twig', [
            'form' => $form->createView()
        ]);
    }
}