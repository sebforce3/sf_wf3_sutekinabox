<?php
/**
 * Created by PhpStorm.
 * User: Etudiant
 * Date: 10/12/2018
 * Time: 15:59
 */

namespace App\Controller;

use App\Entity\MarketingDesigner;
use App\Entity\Member;
use App\Entity\ProductSupplier;
use App\Entity\SalesManager;
use App\Form\LoginFormType;
use App\Form\SubscribeFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class IndexController extends AbstractController
{
    /**
     * Page d'acceuil et de connexion
     * @Route("/", name="sutekina_index")
     *
     * @param AuthenticationUtils $authenticationUtils
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request, AuthenticationUtils $authenticationUtils)
    {
        /**
         * Si notre utilisateur est déjà authentifié,
         * on le redirige vers sa page principale
         */
        if($this->getUser())
        {
            // à faire
            ;
        }
        # Récupération du message d'erreur s'il y en a un.
        $error = $authenticationUtils->getLastAuthenticationError();

        # Dernier email saisie par l'utilisateur.
        $lastEmail = $authenticationUtils->getLastUsername();

        # Formulaire de la page d'accueil, connexion ou inscription
        $login = $this
            ->createForm(
                LoginFormType::class,
                ['email' => $lastEmail] )
            ->handleRequest($request);

        if ($login->isSubmitted() && $login->isValid())
        {
            // rediriger vers la page du rôle
            //return $this->redirect($this->generateUrl('app_product_list'));
        }
        return $this->render('index.html.twig', [
            'form' => $login->createView(),
            'error'         => $error
        ]);
    }


    /**
     * Formulaire de souscription
     * @Route("/subscribe", name="sutekina_subscribe")
     */
    public function subscribe(Request $request)
    {

        $subscribe = $this
            ->createForm(SubscribeFormType::class)
            ->handleRequest($request);

        if ($subscribe->isSubmitted() && $subscribe->isValid())
        {

            // on récupère les données du formulaire
            $data = $subscribe->getData();

            // Inscription de l'utilisateur
            $user = new Member();
            $user->serialize($data);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $id = $user->getId();

            switch ($data['roles']) {
                case 'ROLE_MARKETING_DESIGNER':
                    $backend_user = new MarketingDesigner($id);
                    break;
                case 'ROLE_SALES_MANAGER':
                    $backend_user = new SalesManager($id);
                    break;
                case 'ROLE_PRODUCT_SUPPLIER':
                    $backend_user = new ProductSupplier($id);
                    break;
            }

            $backend_user->setName($data['name']);
            $em->persist($backend_user);
            $em->flush();

            dump($backend_user);

            return new Response("<p>succes</p>");
            //return $this->redirect($this->generateUrl('app_product_list'));
        }

        # Transmission à la Vue
        //'login' =>  $login->createView(),
        return $this->render('inscription.html.twig', [
            'form' => $subscribe->createView(),
        ]);
    }
    /**
     * Déconnexion d'un Membre
     * @Route("/deconnexion", name="sutekina_deconnexion")
     */
    public function deconnexion()
    {
        /**
         * Vous pourriez définir ici,
         * votre logique mot de passe oublié,
         * réinitialisation du mot de passe,
         * Email de validation, ...
         */
    }
}
